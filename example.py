from bs4 import BeautifulSoup
import requests
import pandas as pd
from tkinter import *
from tkinter.messagebox import *

windows_app = Tk()
windows_app.title("Wikipedia Search")
windows_app.geometry ("800x27")
windows_app.resizable (False, False)

string = StringVar()
def result():
    get_string = string.get()
    url = f"https://en.wikipedia.org/wiki/{get_string}"
    req = requests.get (url).text
    soup = BeautifulSoup(req, "html.parser")
    get_data = pd.read_html (str(soup))[0]
    showinfo(title="Result", message=str(get_data))

Entry (textvariable=string, width=110).place (height=25, x=1, y=1)
Button (text="Search", command=result, width=20, height=1).place(x=650, y=1)
windows_app.mainloop()